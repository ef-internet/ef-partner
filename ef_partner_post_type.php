<?php
function register_cpt_ef_partner() {

	/**
	 * Post Type: Partner.
	 */

	$labels = array(
		"name" => __( "Partner", "tm-beans" ),
		"singular_name" => __( "Partner", "tm-beans" ),
		"add_new_item" => __( "Neuen Partner hinzufügen", "tm-beans" ),
		"edit_item" => __( "Partner bearbeiten", "tm-beans" ),
		"view_item" => __( "Partner anzeigen", "tm-beans" ),
	);

	$args = array(
		"label" => __( "Partner", "tm-beans" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "partner", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-admin-links",
		"supports" => array( "title" ),
	);

	register_post_type( "partner", $args );
}


add_action( 'init', 'register_cpt_ef_partner' );
