<?php
function register_ef_partner_kategorie() {

	/**
	 * Taxonomy: Partner-Kategorien.
	 */

	$labels = array(
		"name" => __( "Partner-Kategorien", "tm-beans" ),
		"singular_name" => __( "Partner-Kategorie", "tm-beans" ),
	);

	$args = array(
		"label" => __( "Partner-Kategorien", "tm-beans" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'partner_kategorie', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "partner_kategorie",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
		);
	register_taxonomy( "partner_kategorie", array( "partner" ), $args );
}
add_action( 'init', 'register_ef_partner_kategorie' );