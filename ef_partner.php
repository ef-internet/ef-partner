<?php
/*
Plugin Name: EF Partner
Description: Erstellt einen neuen Post Type 'Partner' mit entsprechenden Input-Feldern zur Dateneingabe und den shortcode ef_partner zur Ausgabe.
Author: Timo Klemm
Version: 0.4.3
Author URI: https://github.com/team-ok
*/

add_action('after_setup_theme', function(){
	if ( ! defined('BEANS_FRAMEWORK_AVAILABLE') ){
		define( 'BEANS_FRAMEWORK_AVAILABLE', function_exists( 'beans_load_document' ) );
	}
});

require_once( plugin_dir_path( __FILE__ ) . 'ef_partner_post_type.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_partner_taxonomies.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_partner_fields.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_partner_styles.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_partner_shortcode.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_partner_item_template.php' );