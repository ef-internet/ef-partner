<?php

add_shortcode( 'ef_partner', 'ef_get_partner' );
function ef_get_partner( $atts, $content = "", $shortcode_tag ){

	$atts = shortcode_atts( array(

		'number' => '-1',
		'order' => 'ASC',
		'orderby' => 'title',
		'category' => '',
		'include' => '',
		'exclude' => '',
		'title' => 'false',
		'text' => 'false',
		'fullwidth' => 'false',
		'link' => 'external', // 'internal' => internal partner page, <URL> => manually entered URL
		'columns_max' => 6,
		'columns_min' => 2,
		'maxheight' => null,
		'align' => 'middle', // top, bottom, middle

	), $atts, $shortcode_tag );

	$args = array(
		'post_type' => 'partner',
		'numberposts' => $atts['number'],
		'order' => $atts['order'],
		'orderby' => $atts['orderby'],
		'include' => $atts['include'],
		'exclude' => $atts['exclude']
	);

	if ( $atts['category']){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'partner_kategorie',
				'field' => 'slug',
				'terms' => explode(',', $atts['category'])
			)
		);
	}

	$partner = get_posts( $args );

	if ( ! $partner ){
		return '';
	}

	$partner_class = $atts['text'] != 'false' || $atts['fullwidth'] != 'false' ? ' ef-partner-full-width' : ' uk-grid uk-grid-width-1-' . $atts['columns_min'] . ' uk-grid-width-medium-1-' . ceil($atts['columns_max'] / 2) . ' uk-grid-width-xlarge-1-' . $atts['columns_max'];
	$partner_class .= ' uk-flex-' . esc_attr($atts['align']);
	$partner_atts = $atts['fullwidth'] == 'false' ? ' data-uk-grid-margin' : '';

	ob_start(); ?>

	<div class="ef-partner<?php echo $partner_class; ?>"<?php echo $partner_atts; ?>>

		<?php global $post;

		foreach ($partner as $post):
			
			setup_postdata( $post );

			$logo = get_field( 'partner-logo' ); // array
			$text = '';
			$title = '';
			$link_atts = '';

			if ( $atts['text'] != 'false' || $atts['link'] == 'internal' ){
				$text = get_field( 'partner-text' );
			}
			if ( $atts['title'] != 'false' ){
				$title = get_the_title();
			}
			if ( $atts['link'] === 'external' ){
				$link = get_field( 'partner-url' );
				$link_atts .= ' target="_blank" rel="noopener noreferrer"';
			} elseif ( $atts['link'] === 'internal') {
				if ( $atts['text'] == 'false' && !empty($text) ){
					$link = get_permalink();
				} else {
					$link = get_field( 'partner-url' );
					$link_atts .= ' target="_blank" rel="noopener noreferrer"';
				}
			} else {
				$link = $atts['link'];
			}
			$max_height = is_numeric( $atts['maxheight'] ) ? ' style="max-height: '.$atts['maxheight'].'px;"' : '';

			?>

			<div id="<?php echo sanitize_title( get_the_title() ); ?>" class="ef-partner-item">
				<?php if ( $link ){ ?>
					<a href="<?php echo esc_url( $link ); ?>"<?php echo $link_atts; ?>>
				<?php }
					if ( $title ){
						echo '<h4 class="ef-partner-title">' . $title . '</h4>';
					} ?>
						<img<?php echo $max_height; ?> src="<?php echo $logo['url']; ?>" width="<?php echo $logo['width']; ?>" height="<?php echo $logo['height']; ?>" alt="<?php echo esc_attr( $logo['alt'] ); ?>">
				<?php if ( $link ){ ?>
					</a>
				<?php } ?>
				<?php if ( $atts['text'] != 'false' && !empty($text) ){
					echo apply_filters( 'the_content', $text );
				} ?>
			</div>

		<?php endforeach;

		wp_reset_postdata(); ?>

	</div>

	<?php
	// enqueue frontend assets
	if ( ! BEANS_FRAMEWORK_AVAILABLE ){
		wp_enqueue_script( 'uikit' );

		wp_enqueue_style( 'uikit' );
		wp_enqueue_style( 'ef-partner' );
	}
	
	return ob_get_clean();
}