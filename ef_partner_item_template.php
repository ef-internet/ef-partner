<?php
add_action( 'beans_post_content_prepend_markup', function(){

	if ( 'partner' === get_post_type() ){

		$logo = get_field( 'partner-logo' ); // array
		$link = get_field( 'partner-url' );
		$text = get_field( 'partner-text' );
		?>
		<div class="ef-partner ef-partner-full-width">
			<div class="ef-partner-item">
				<?php if ($link){ ?>
					<a href="<?php echo esc_url( $link ); ?>" target="_blank" rel="noopener noreferrer">
				<?php } ?>
					<?php if ( $logo ){ ?>
						<img src="<?php echo $logo['url']; ?>" width="<?php echo $logo['width']; ?>" height="<?php echo $logo['height']; ?>" alt="<?php echo esc_attr( $logo['alt'] ); ?>">
					<?php } ?>
				<?php if ($link){ ?>
					</a>
				<?php } ?>
				<?php if ( $text ){
					echo apply_filters( 'the_content', $text );
				} ?>
			</div>
		</div>
	<?php }
});