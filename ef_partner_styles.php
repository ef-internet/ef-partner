<?php

add_action( 'beans_uikit_enqueue_scripts', 'ef_partner_enqueue_less_fragment' );

function ef_partner_enqueue_less_fragment(){
	beans_compiler_add_fragment( 'uikit', plugin_dir_path( __FILE__ ) . '/styles/ef_partner.less', 'less' );
}

add_action('wp_enqueue_scripts', function(){

	if ( ! BEANS_FRAMEWORK_AVAILABLE ){
		wp_register_script( 'uikit', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/js/uikit.min.js', array( 'jquery' ) );
		wp_register_style( 'uikit', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.5/css/uikit.min.css' );
		wp_register_style( 'ef-partner', plugin_dir_url( __FILE__ ) . 'styles/ef-partner.css' );
	}
});